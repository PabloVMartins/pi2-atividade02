const express = require("express");
const routes = express.Router()

const JogoController = require("./src/controllers/JogoController");
const DisponibilidadeController = require("./src/controllers/DisponibilidadeController");
const UsuarioController = require("./src/controllers/UsuarioController");
const EstatisticaController = require("./src/controllers/EstatisticaController");
const login = require("./src/middleware/login")

routes.post("/jogos", JogoController.store)
      .get("/jogos", JogoController.index)
      .put("/jogos/:titulo", JogoController.update)
      .delete("/jogos/delete/:titulo", JogoController.destroy)
      .get("/jogos/destaques", JogoController.destaques)
      .put("/jogos/destaques/destacar/:titulo", JogoController.destacar);

routes.get("/usuarios", UsuarioController.index)
      .post("/usuarios", UsuarioController.store)
      .post("/login", UsuarioController.login);

routes.get("/disponibilidade", DisponibilidadeController.index)
      .post("/disponibilidade", DisponibilidadeController.create)
      .put("/disponibilidade/:jogo_id", DisponibilidadeController.update)
      .delete("/disponibilidade/:jogo_id", DisponibilidadeController.destroy);

routes.get("/estatistica", EstatisticaController.index);

module.exports = routes;