import request from 'supertest';

import app from '../../app';

let jogos;

beforeEach(() => {

    jogos = [{
        titulo: "GTA V",
        genero_id: 4,
        distribuidora_id: 4,
        ano: 2013,
        preco: "99.05",
        foto: "https://www.rockstargames.com/V/img/global/order/mobile-cover.jpg"
    }, {
        titulo: "Valheim",
        genero_id: 2,
        distribuidora_id: 2,
        ano: 2021,
        preco: "40.00",
        foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqVEgQIczMaWTk9CNDjQPGt400EJFrnoinfQ&usqp=CAU"
    }]

})

test('Deve ser possivel adicionar um novo jogo', async () => {
    const response = await request(app).post('/jogos').send(jogos[0]);
    
    expect(response.body).toMatchObject([jogos[0]]);
    
    await request(app).delete("/jogos/delete/GTA V");
});

test('Deve receber uma mensagem de erro ao tentar incluir um jogo faltando algo', async () => {
    const response = await request(app).post('/jogos').send(jogos[2]);

    expect(response.status).toBe(400);

})

test('Deve ser possivel alterar um jogo', async () => {
    await request(app).post('/jogos').send(jogos[0]);

    const response = await request(app).put("/jogos/GTA V").send(jogos[1]);
    
    expect(response.body).toBe("Jogo alterado com sucesso.");
    
    await request(app).delete("/jogos/delete/Valheim");
});

test('Deve receber um erro caso não encontre o jogo para alterar', async () => {
    await request(app).post('/jogos').send(jogos[0]);
    
    const response = await request(app).put("/jogos/Valheim").send(jogos[1]);

    expect(response.body).toBe("Jogo não encontrado.")

    await request(app).delete("/jogos/delete/GTA V");
})

test('Deve ser possivel excluir um jogo', async () => {
    await request(app).post('/jogos').send(jogos[0]);

    const response = await request(app).delete("/jogos/delete/GTA V");

    expect(response.body).toBe("Jogo excluído com sucesso.");
});

test('Deve receber um erro caso não encontre o jogo para deletar', async ()=> {
    await request(app).post('/jogos').send(jogos[0]);

    const response = await request(app).delete("/jogos/delete/Valheim");

    expect(response.body).toBe("Jogo não encontrado.");

    await request(app).delete("/jogos/delete/GTA V");
})

test('Deve ser possivel destacar um jogo', async () => {
    await request(app).post('/jogos').send(jogos[0]);

    const response = await request(app).put('/jogos/destaques/destacar/GTA V').send({ destaque: true });

    expect(response.body).toBe("Jogo adicionado aos destaques.");

    await request(app).delete("/jogos/delete/GTA V");
});

test('Deve ser possivel listar todos os itens em destaque', async () => {
    await request(app).post('/jogos').send(jogos[0]);
    
    await request(app).put('/jogos/destaques/destacar/GTA V').send({ destaque: true });

    const response = await request(app).get("/jogos/destaques");

    expect(response.body).toMatchObject([jogos[0]]);

    await request(app).delete("/jogos/delete/GTA V");
});

test('Deve receber uma mensagem de erro ao tentar destacar um jogo que nao existe', async () => {
    await request(app).post('/jogos').send(jogos[0]);

    const response = await request(app).put('/jogos/destaques/destacar/Valheim').send({ destaque: true });

    expect(response.body).toBe("Jogo não encontrado.");

    await request(app).delete("/jogos/delete/GTA V");
});

test('Deve ser possivel consultar os jogos', async () => {
    await request(app).post('/jogos').send(jogos[0]);
    await request(app).post('/jogos').send(jogos[1]);

    const response = await request(app).get('/jogos');

    expect(response.body).toMatchObject([{ ...jogos[1] }, { ...jogos[0] }]);

    await request(app).delete("/jogos/delete/GTA V");
    await request(app).delete("/jogos/delete/Valheim");
});
