const knex = require("../database/dbConfig");

module.exports = {

  // index: listagem
  // store/create: inclusão
  // update: alteração
  // show: retornar 1 registro
  // destroy: exclusão

  async index(req, res) {

    const jogos = await knex("jogos");

    res.status(200).json(jogos);
  },

  async store(req, res) {

    const { titulo, genero_id, distribuidora_id, ano, preco, foto } = req.body;

    if (!titulo || !genero_id || !distribuidora_id || !ano || !preco || !foto) {
      res.status(400).json({ msg: "Faltou algo" });
      return;
    }

    try {
      const novo = await knex("jogos").insert({ titulo, genero_id, distribuidora_id, ano, preco, foto });
      const retorno = await knex("jogos").select("titulo", "genero_id", "distribuidora_id", "ano", "preco", "foto").where({ id: novo[0] })

      res.status(201).json(retorno);
    } catch (error) {
      res.status(400).json({ msg: error.message });
    }
  },

  async update(req, res) {

    const { titulo } = req.params;
    const { genero_id, distribuidora_id, ano, preco } = req.body;

    const novo = await knex("jogos").where({ titulo })

    if (!novo[0]) {
      res.status(400).json("Jogo não encontrado.")
      return;
    }

    try {
      await knex("jogos").update({ genero_id, distribuidora_id, ano, preco }).where({ titulo })
      res.status(200).json("Jogo alterado com sucesso.")
    } catch (error) {
      res.status(400).json({ msg: error.message })
    }
  },

  async destroy(req, res) {

    const { titulo } = req.params

    const novo = await knex("jogos").where({ titulo })

    if (!novo[0]) {
      res.status(400).json("Jogo não encontrado.")
      return;
    }

    try {
      await knex("jogos").del().where({ titulo })
      return res.status(200).json("Jogo excluído com sucesso.")
    } catch (error) {
      res.status(400).json({ msg: error.message })
    }
  },

  async destaques(req, res) {

    try {
      const novo = await knex("jogos").where("destaque", "=", true)

      if (novo.length >= 1) {
        res.status(200).json(novo)
        return;
      }
      else {
        res.status(400).json("Não há jogos em destaque.")
      }

    } catch (error) {
      res.status(400).json({ msg: error.message })
    }
  },

  async destacar(req, res) {

    const { titulo } = req.params
    const novo = await knex("jogos").where({ titulo })

    if (!novo[0]) {
      res.status(400).json("Jogo não encontrado.")
      return;
    }

    try {
      if (novo[0].destaque) {
        await knex("jogos").where({ titulo }).update({ destaque: false })
        res.status(200).json("Jogo removido dos destaques.")
      }
      else {
        await knex("jogos").where({ titulo }).update({ destaque: true })
        res.status(200).json("Jogo adicionado aos destaques.")
      }
    } catch (error) {
      res.status(400).json({ msg: error.message })
    }
  }
};